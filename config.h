/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 0;
static const unsigned int snap      = 10;
static const int rmaster            = 0;
static const int showbar            = 1;
static const int topbar             = 0;
static const int user_bh            = 35;
static const char *fonts[]          = {"Source Code Pro:size=14",
                                       "FontAwesome:size=14",
                                       "Source Han Sans JP Medium:size=14"};
static const char dmenufont[]       = "Source Code Pro:size=14";
static char normfgcolor[]           = "#ffffff";
static char normbgcolor[]           = "#000000";
static char normbordercolor[]       = "#ff0000";
static char selfgcolor[]            = "#ee4444";
static char selbordercolor[]        = "#ff0000";
static char selbgcolor[]            = "#000000";
static char *colors[][3]            = {
  /*               fg           bg           border   */
  [SchemeNorm]     = { normfgcolor, normbgcolor, normbordercolor},
  [SchemeSel]      = {  selfgcolor,  selbgcolor,  selbordercolor},
  [SchemeStatus]   = { normfgcolor, normbgcolor,       "#000000"},// Statusbar right
  [SchemeTagsSel]  = {  selfgcolor,  selbgcolor,       "#000000"},// Tagbar left selected 
  [SchemeTagsNorm] = { normfgcolor, normbgcolor,       "#000000"},// Tagbar left unselected 
  [SchemeInfoSel]  = { normfgcolor, normbgcolor,       "#000000"},// infobar middle selected 
  [SchemeInfoNorm] = { normfgcolor, normbgcolor,       "#000000"},// infobar middle unselected
};

static const char *const autostart[] = {
  "dunst", NULL,
  "/home/mk/.fehbg", NULL,
  //"xsetrot", "-solid", "#xfbcbb", NULL,
  //"emacs" ,"--daemon", NULL,
  "picom", "--daemon", "--config", "/home/mk/.config/picom.conf", NULL,
  //"xautolock", "-time", "30", "-locker", "slock", NULL,
  //"/home/mk/.local/bin/battery-status", NULL,
  "/home/mk/.local/bin/dwm-bar", NULL,
  "/home/mk/.local/bin/layout", NULL,
  "/home/mk/.local/bin/screenlayout", "prev", NULL,
  //"redshift", "-l", "-34.59:-58.38", "-t" , "6500:4500", NULL,
  "/home/mk/.local/bin/special_programs", NULL,
  NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", ""};
static const char floating[] = "floating";
static const Rule rules[] = {
  /* xprop(1):
   *  WM_CLASS(STRING) = instance, class
   *  WM_NAME(STRING) = title */
  /* class                     instance     title  tags mask  isfloating   monitor */
  { "URxvt",                   floating,     NULL,         0,          1,       -1},
  { "kitty",                       NULL, floating,         0,          1,       -1},
  { "feh",                         NULL,     NULL,         0,          1,       -1},
  { "Tor Browser",                 NULL,     NULL,    1 << 1,          0,       -1},
  { "firefox",                     NULL,     NULL,    1 << 1,          0,       -1},
  { "firefox",                 "Places",     NULL,    1 << 1,          1,       -1},
  { "VSCodium",                    NULL,     NULL,    1 << 2,          0,       -1},
  { "Emacs",                       NULL,     NULL,    1 << 2,          0,       -1},
  { "Simple-scan",                 NULL,     NULL,    1 << 4,          0,       -1},
  { "pavucontrol-qt",              NULL,     NULL,    1 << 4,          1,       -1},
  { "Zathura",                     NULL,     NULL,    1 << 4,          0,       -1},
  { "libreoffice-startcenter",     NULL,     NULL,    1 << 4,          0,       -1},
  { "Inkscape",                    NULL,     NULL,    1 << 5,          0,       -1},
  { "Blender",                     NULL,     NULL,    1 << 5,          0,       -1},
  { "Gimp-2.10",                   NULL,     NULL,    1 << 5,          0,       -1},
  { "Quodlibet",                   NULL,     NULL,    1 << 5,          0,       -1},
  { "vlc",                         NULL,     NULL,    1 << 5,          0,       -1},
  { "sunvox",                      NULL,     NULL,    1 << 5,          0,       -1},
  { "Audacity",                    NULL,     NULL,    1 << 5,          0,       -1},
  { "Slack",                       NULL,     NULL,    1 << 6,          0,       -1},
  { "discord",                     NULL,     NULL,    1 << 6,          0,       -1},
  { "War Thunder",                 NULL,     NULL,    1 << 7,          0,       -1},
  { "VirtualBox Machine",          NULL,     NULL,    1 << 8,          0,       -1},
  { "VirtualBox Manager",          NULL,     NULL,    1 << 8,          1,       -1},
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;   /* number of clients in master area */
static const int resizehints = 0;   /* 1 means respect size hints in tiled resizals */

//#include "tcl.c"
static const Layout layouts[] = {
  { "[]=",  tile},
  { "<><",  NULL},
  { "[M]",  monocle},
  { "[D]",  deck},
  //{ "|3|",  tcl},
  { "|M|",  centeredmaster},
  { ">M>",  centeredfloatingmaster},
  { NULL,   NULL},
};

/* key definitions */
#define MODKEY Mod4Mask
#define SHFT ShiftMask
#define CTRL ControlMask
#define TAGKEYS(KEY,TAG) \
  { MODKEY,           KEY, view,       {.ui = 1 << TAG}}, \
  { MODKEY CTRL,      KEY, toggleview, {.ui = 1 << TAG}}, \
  { MODKEY SHFT,      KEY, tag,        {.ui = 1 << TAG}}, \
  { MODKEY CTRL SHFT, KEY, toggletag,  {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL}}

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] =  {"dmenu_run", "-m", dmenumon,
                                     "-i",
                                     "-fn", dmenufont,
                                     "-nb", normbgcolor,
                                     "-nf", normfgcolor,
                                     "-sb", selbgcolor,
                                     "-sf", selfgcolor, NULL};
//static const char *term[]     =  {"urxvt", NULL};
//static const char *term2[]    =  {"urxvt", "-name" , floating, "--geometry", "80x20", NULL};
//static const char *term[]     =  {"kitty", NULL};
//static const char *term2[]    =  {"kitty", "--title" , floating, NULL};
static const char *term[]     =  {"xfce4-terminal", NULL};
static const char *term2[]    =  {"xfce4-terminal", "--title" , floating, NULL};
static const char *web[]      =  {"firefox", NULL};
static const char *todocmd[]  =  {"/home/mk/.local/bin/todo", NULL};
static const char *scrot[]    =  {"/home/mk/.local/bin/scrot-notify", NULL};
static const char *scrnlyt[]  =  {"/home/mk/.local/bin/screenlayout", NULL};
static const char *code[]     =  {"/home/mk/.local/bin/code", NULL};
static const char *emacs[]    =  {"/home/mk/.local/bin/my_emacs", NULL};
//static const char *lock[]     =  {"/usr/local/bin/slock", NULL};
//static const char *suspend[]  =  {"systemctl", "suspend", NULL};
//static const char *poweroff[] =  {"poweroff", NULL};
static const char *session_options[]  =  {"/home/mk/.local/bin/session_options", NULL};
static const char *kbd[][2]   = {{"/home/mk/.local/bin/kb-light.py", "+"},
                                 {"/home/mk/.local/bin/kb-light.py", "-"}};
static const char *mon[][2]   = {{"/home/mk/.local/bin/brightness", "+"},
                                 {"/home/mk/.local/bin/brightness", "-"}};
static const char *vol[][3]   = {{"/home/mk/.local/bin/volume", "-"},
                                 {"/home/mk/.local/bin/volume", "0"},
                                 {"/home/mk/.local/bin/volume", "+"}};

#define XK_MBrUp     0x1008FF02
#define XK_MBrDown   0x1008FF03
#define XK_KBrUp     0x1008FF05
#define XK_KBrDown   0x1008FF06
#define XK_AuLower   0x1008FF11
#define XK_AuMute    0x1008FF12
#define XK_AuRaise   0x1008FF13

#include "movestack.c"
static Key keys[] = {
  /* modifier         key        function        argument */
  { MODKEY,           XK_space,  spawn,          {.v = dmenucmd}},
  { MODKEY,           XK_Return, spawn,          {.v = term}},
  { MODKEY,           XK_a,      spawn,          {.v = term2}},
  { MODKEY,           XK_F1,     spawn,          {.v = web}},
  { MODKEY,           XK_F3,     spawn,          {.v = emacs}},
  { MODKEY|SHFT,      XK_F3,     spawn,          {.v = code}},
  { MODKEY|SHFT,      XK_d,      spawn,          {.v = todocmd}},
  //{ CTRL|SHFT,        XK_l,      spawn,          {.v = lock}},
  //{ MODKEY|CTRL|SHFT, XK_t,      spawn,          {.v = suspend}},
  //{ MODKEY|CTRL|SHFT, XK_p,      spawn,          {.v = poweroff}},
  { MODKEY|SHFT,      XK_p,      spawn,          {.v = session_options}},
  { MODKEY,           XK_p,      spawn,          {.v = scrnlyt}},
  { MODKEY,           XK_b,      togglebar,      {0}},
  { MODKEY,           XK_r,      togglermaster,  {0}},
  { MODKEY|SHFT,      XK_r,      rotatestack,    {.i = -1}},
  { MODKEY,           XK_k,      focusstack,     {.i = +1}},
  { MODKEY,           XK_l,      focusstack,     {.i = -1}},
  { MODKEY,           XK_i,      incnmaster,     {.i = +1}},
  { MODKEY|SHFT,      XK_i,      incnmaster,     {.i = -1}},
  { MODKEY,           XK_j,      setmfact,       {.f = -0.05}},
  { MODKEY,           XK_minus,  setmfact,       {.f = +0.05}},
  { MODKEY|SHFT,      XK_k,      movestack,      {.i = +1}},
  { MODKEY|SHFT,      XK_l,      movestack,      {.i = -1}},
  { MODKEY,           XK_z,      zoom,           {0}},
  { MODKEY|SHFT,      XK_q,      killclient,     {0}},
  { MODKEY,           XK_t,      setlayout,      {.v = &layouts[0]}},
  { MODKEY,           XK_f,      setlayout,      {.v = &layouts[1]}},
  { MODKEY,           XK_m,      setlayout,      {.v = &layouts[2]}},
  { MODKEY,           XK_Tab,    cyclelayout,    {.i = +1}},
  { MODKEY|SHFT,      XK_Tab,    cyclelayout,    {.i = -1}},
  { MODKEY,           XK_s,      togglefloating, {0}},
  { MODKEY|SHFT,      XK_s,      togglesticky,   {0}},
  { MODKEY,           XK_0,      view,           {.ui = ~0}},
  { MODKEY|SHFT,      XK_0,      tag,            {.ui = ~0}},
  { MODKEY,           XK_comma,  focusmon,       {.i = -1}},
  { MODKEY,           XK_period, focusmon,       {.i = +1}},
  { MODKEY|SHFT,      XK_comma,  tagmon,         {.i = -1}},
  { MODKEY|SHFT,      XK_period, tagmon,         {.i = +1}},
  { MODKEY,           XK_F5,     xrdb,           {.v = NULL}},
  TAGKEYS(            XK_1,                      0)
  TAGKEYS(            XK_2,                      1)
  TAGKEYS(            XK_3,                      2)
  TAGKEYS(            XK_4,                      3)
  TAGKEYS(            XK_5,                      4)
  TAGKEYS(            XK_6,                      5)
  TAGKEYS(            XK_7,                      6)
  TAGKEYS(            XK_8,                      7)
  TAGKEYS(            XK_9,                      8)
  { MODKEY|SHFT,      XK_e,      quit,           {0}},
  { 0,                XK_MBrUp,  spawn,          {.v = mon[0]}},
  { 0,                XK_MBrDown,spawn,          {.v = mon[1]}},
  { 0,                XK_KBrUp,  spawn,          {.v = kbd[0]}},
  { 0,                XK_KBrDown,spawn,          {.v = kbd[1]}},
  { 0,                XK_AuLower,spawn,          {.v = vol[0]}},
  { 0,                XK_AuMute, spawn,          {.v = vol[1]}},
  { 0,                XK_AuRaise,spawn,          {.v = vol[2]}},
  { 0,                XK_Print,  spawn,          {.v = scrot}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click         event mask button   function        argument */
  { ClkLtSymbol,   0,         Button1, setlayout,      {0}},
  { ClkLtSymbol,   0,         Button3, setlayout,      {.v = &layouts[2]}},
  { ClkWinTitle,   0,         Button2, zoom,           {0}},
  { ClkStatusText, 0,         Button2, spawn,          {.v = term}},
  { ClkClientWin,  MODKEY,    Button1, movemouse,      {0}},
  { ClkClientWin,  MODKEY,    Button2, togglefloating, {0}},
  { ClkClientWin,  MODKEY,    Button3, resizemouse,    {0}},
  { ClkTagBar,     0,         Button1, view,           {0}},
  { ClkTagBar,     0,         Button3, toggleview,     {0}},
  { ClkTagBar,     MODKEY,    Button1, tag,            {0}},
  { ClkTagBar,     MODKEY,    Button3, toggletag,      {0}},
};


static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  xrdb,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   )
};



