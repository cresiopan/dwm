# dwm

Patches applied:
- [ipc](https://dwm.suckless.org/patches/ipc/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
- [colorbar](https://dwm.suckless.org/patches/colorbar/)
- [xrdb](https://dwm.suckless.org/patches/xrdb/)
- [rmaster](https://dwm.suckless.org/patches/rmaster/)
- [bar height](https://dwm.suckless.org/patches/bar_height/)