/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 0;
static const unsigned int snap      = 10;
static const int showbar            = 1;
static const int topbar             = 0;
static const char *fonts[]          = { "Source Code Pro:size=12", "Source Han Sans JP Medium:size=12", "Unifont:size=12", "Font Awesome 5 Free:size=12" };
static const char dmenufont[]       = "Source Code Pro:size=12";
static const char normfgcolor[]     = "#ffffff";
static const char normbgcolor[]     = "#000000";
static const char normbordercolor[] = "#ff0000";
static const char selfgcolor[]      = "#ee4444";
static const char selbordercolor[]  = "#ff0000";
static const char selbgcolor[]      = "#000000";
static const char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeStatus] = { normfgcolor, normbgcolor, "#000000" }, // Statusbar right {fg,bg,not used but cannot be empty}
       [SchemeTagsSel] = { selfgcolor, selbgcolor, "#000000" }, // Tagbar left selected {fg,bg,not used but cannot be empty}
       [SchemeTagsNorm] = { normfgcolor, normbgcolor, "#000000" }, // Tagbar left unselected {fg,bg,not used but cannot be empty}
       [SchemeInfoSel] = { selfgcolor, selbgcolor, "#000000" }, // infobar middle selected {fg,bg,not used but cannot be empty}
       [SchemeInfoNorm] = { normfgcolor, normbgcolor, "#000000" }, // infobar middle unselected {fg,bg,not used but cannot be empty}
};

static const char *const autostart[] = {
	"dunst", NULL,
	"/home/mk/.fehbg", NULL,
	"picom", "--daemon", "--config", "/home/mk/.config/picom.conf", NULL,
    "xautolock", "-time", "10", "-locker", "slock", NULL,
	"/home/mk/.local/bin/xcv", "seapunk", NULL,
	"/home/mk/.local/bin/battery-status", NULL,
	"/home/mk/.local/bin/dwm-bar", NULL,
	"/home/mk/.local/bin/layout", NULL,
	"/home/mk/.local/bin/screenlayout", "prev", NULL,
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "home", "web", "edu", "4", "5", "6", "7", "8", "work" };
//static const char *tags[] = { "月","火","水","木","金","土","日" };

static const char scratchpad[] = "floatingTerminal";
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "VirtualBox Machine", NULL, NULL, 1 << 8, 0, -1 },
	{ "VirtualBox Manager", NULL, NULL, 1 << 8, 1, -1 },
    { "firefox", NULL, NULL, 1 << 1, 0, -1 },
	{ "URxvt", scratchpad, NULL, 0, 1, -1 },
	{ "feh", NULL, NULL, 0, 1, -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;   /* number of clients in master area */
static const int resizehints = 0;   /* 1 means respect size hints in tiled resizals */

#include "tcl.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "|||",      tcl },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "urxvt", NULL };
static const char *termfloatingcmd[]  = { "urxvt", "-name" , scratchpad, "-geometry", "80x20", NULL };

static const char *webcmd[]  = { "firefox", NULL };
static const char *todocmd[] = { "/home/mk/.local/bin/todo", NULL };
static const char *scrot[]    =  { "/home/mk/.local/bin/scrot-notify", NULL};
static const char *lock[]     =  { "/usr/local/bin/slock", NULL};
static const char *code[]     =  { "code", NULL};
static const char *kbd[][2]   = {{ "/home/mk/.local/bin/kb-light.py", "+" },
                                 { "/home/mk/.local/bin/kb-light.py", "-" }};
static const char *mon[][2]   = {{ "/home/mk/.local/bin/brightness", "+" },
                                 { "/home/mk/.local/bin/brightness", "-" }};
static const char *vol[][3]   = {{ "/home/mk/.local/bin/volume", "-" },
                                 { "/home/mk/.local/bin/volume", "0" },
                                 { "/home/mk/.local/bin/volume", "+" }};

#define XK_MBrUp     0x1008FF02
#define XK_MBrDown   0x1008FF03
#define XK_KBrUp     0x1008FF05
#define XK_KBrDown   0x1008FF06
#define XK_AuLower   0x1008FF11
#define XK_AuMute    0x1008FF12
#define XK_AuRaise   0x1008FF13

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_space,  spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_a,      spawn,          {.v = termfloatingcmd } },
	{ MODKEY,                       XK_F1,     spawn,          {.v = webcmd } },
	{ MODKEY,                       XK_F3,     spawn,          {.v = code } },
	{ MODKEY|ShiftMask,             XK_d,      spawn,          {.v = todocmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_r,      rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_r,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_l,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_j,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_minus,  setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_l,      movestack,      {.i = -1 } },
	{ ControlMask|ShiftMask,        XK_l,      spawn,          {.v = lock } },
	{ MODKEY,                       XK_z,      zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_Tab,    cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Tab,    cyclelayout,    {.i = -1 } },
	{ MODKEY,                       XK_s,      togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_e,      quit,           {0} },
    	{ 0,                            XK_MBrUp,  spawn,          {.v = mon[0] } },
    	{ 0,                            XK_MBrDown,spawn,          {.v = mon[1] } },
    	{ 0,                            XK_KBrUp,  spawn,          {.v = kbd[0] } },
    	{ 0,                            XK_KBrDown,spawn,          {.v = kbd[1] } },
    	{ 0,                            XK_AuLower,spawn,          {.v = vol[0] } },
    	{ 0,                            XK_AuMute, spawn,          {.v = vol[1] } },
    	{ 0,                            XK_AuRaise,spawn,          {.v = vol[2] } },
    	{ 0,                            XK_Print,  spawn,          {.v = scrot } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

